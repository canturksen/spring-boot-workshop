package com.opsgenie.workshop.demo;

import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.team.repository.TeamDynamoRepoImpl;
import com.opsgenie.workshop.demo.team.repository.TeamRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TeamDynamoRepoExtTest {

    private TeamRepository repository;

    @Before
    public void setUp() {
        repository = new TeamDynamoRepoImpl();
    }

    @After
    public void tearDown() {
        repository = null;
    }

    @Test
    public void addListTeamsTest() throws Exception {
        List<Team> expectedTeams = Arrays.asList(
                TeamFixture.unique(),
                TeamFixture.unique()
        );

        repository.addTeam(expectedTeams.get(0));
        repository.addTeam(expectedTeams.get(1));


        List<Team> teamList = repository.listTeams();
        assertThat( teamList, hasItem(expectedTeams.get(0)));
        assertThat( teamList, hasItem(expectedTeams.get(1)));
    }

    @Test
    public void addGetDeleteTeamTest() throws Exception {
        Team expectedTeam = TeamFixture.unique();

        repository.addTeam(expectedTeam);

        Team teamRetrieved = repository.getTeam(expectedTeam);
        assertThat(teamRetrieved, is(equalTo(expectedTeam)));

        repository.deleteTeam(teamRetrieved);

        List<Team> teamList = repository.listTeams();
        assertThat(teamList, not(hasItem(teamRetrieved)));
    }

    @Test
    public void addUpdateTeamTest() throws Exception {
        Team expectedTeam = TeamFixture.unique();

        repository.addTeam(expectedTeam);

        Team teamRetrieved = repository.getTeam(expectedTeam);
        assertThat(teamRetrieved, is(equalTo(expectedTeam)));

        Team updateTeam = TeamFixture.unique();
        expectedTeam.setName(updateTeam.getName());

        repository.updateTeam(expectedTeam);

        List<Team> teamList = repository.listTeams();
        assertThat(teamList, not(hasItem(teamRetrieved)));
    }

}
