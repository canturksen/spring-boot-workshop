package com.opsgenie.workshop.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.team.controller.TeamController;
import com.opsgenie.workshop.demo.team.dto.TeamDTO;
import com.opsgenie.workshop.demo.team.service.TeamService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamController.class)
public class TeamControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamService teamService;

    @Test
    public void getTeamsEmpty() throws Exception {
        when(teamService.listTeams()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/teams"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void getTeamsNotEmpty() throws Exception {
        Team expectedTeam1 = new Team("id1", "team1", Stream.of("user1","user3").collect(Collectors.toSet()));
        Team expectedTeam2 = new Team("id2", "team2", Stream.of("user3","user0").collect(Collectors.toSet()));
        List<Team> outputArray = Arrays.asList(expectedTeam1, expectedTeam2);

        when(teamService.listTeams()).thenReturn(outputArray);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(outputArray);

        mockMvc.perform(get("/teams"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));

        verify(teamService).listTeams();
    }

    @Test
    @Ignore
    public void postTeam() throws Exception {
        //Team expectedTeam = new Team("id1", "team1", Stream.of("user1","user3").collect(Collectors.toSet()));
        TeamDTO expectedTeamDTO = new TeamDTO("id1", "team1", Stream.of("user1","user3").collect(Collectors.toSet()));

        when(teamService.addTeam(expectedTeamDTO)).thenReturn(expectedTeamDTO.id);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(expectedTeamDTO);

        mockMvc.perform(post("/teams").contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().json(requestJson));
    }
}
