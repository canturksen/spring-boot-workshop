package com.opsgenie.workshop.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.opsgenie.workshop.demo.aws.AssignTeamMemberQueueProcessor;
import com.opsgenie.workshop.demo.aws.QueueMessageDTO;
import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.team.repository.TeamRepository;
import com.opsgenie.workshop.demo.team.service.TeamService;
import com.opsgenie.workshop.demo.team.service.TeamServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceImplTest {

    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private TeamRepository teamRepository;

    @Mock
    private AssignTeamMemberQueueProcessor assignTeamMemberQueueProcessor;

    @After
    public void tearDown() {
        teamRepository = null;
        teamService = null;
    }

    @Test
    public void listTeams() throws Exception {
        final List<Team> expectedTeams = Arrays.asList(
                new Team("id1", "team1", Stream.of("user1", "user2").collect(Collectors.toSet())),
                new Team("id2", "team2", Stream.of("user1", "user3").collect(Collectors.toSet()))
        );

        when(teamRepository.listTeams()).thenReturn(expectedTeams);

        final List<Team> teams = teamService.listTeams();

        assertThat(teams, is(equalTo(expectedTeams)));

        verify(teamRepository, times(1)).listTeams();
    }

    @Test
    public void assignMemberTest() throws JsonProcessingException {
        final List<String> expectedUserList = Arrays.asList("usr1", "usr2");
        final String expectedTeamId = "team1";

//        Team t=mock(Team.class);
//        when(teamRepository.getTeam(t)).thenReturn(t);

        teamService.addMember(new QueueMessageDTO().setTeamId(expectedTeamId).setUserId(expectedUserList.get(0)));
        teamService.addMember(new QueueMessageDTO().setTeamId(expectedTeamId).setUserId(expectedUserList.get(1)));

        verify(teamRepository, times(2)).getTeam(any());
        //verify(teamRepository, times(2)).updateTeam(any());

    }
}
