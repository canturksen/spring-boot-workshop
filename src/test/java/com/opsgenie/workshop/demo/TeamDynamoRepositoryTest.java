package com.opsgenie.workshop.demo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.team.repository.TeamDynamoRepoImpl;
import com.opsgenie.workshop.demo.team.repository.TeamRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TeamDynamoRepositoryTest {

    TeamRepository repository;

    @Before
    public void setUp() {
        repository = new TeamDynamoRepoImpl();
    }

    @After
    public void tearDown() {
        repository = null;
    }

    @Test
    @Ignore
    public void listTeams() {
        List<Team> expectedTeams = Arrays.asList(
                TeamFixture.unique(),
                TeamFixture.unique()
        );
        //when(repository.scan(eq(Team.class), any(DynamoDBScanExpression.class))).thenReturn(expectedTeams);

        final List<Team> teams = repository.listTeams();

        assertThat(teams, is(equalTo(expectedTeams)));
        //verify(accessor, times(1)).scan(eq(Team.class), any(DynamoDBScanExpression.class));
    }

    @Test
    @Ignore
    public void getTeam() {

    }
}
