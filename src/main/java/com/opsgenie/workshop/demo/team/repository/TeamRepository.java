package com.opsgenie.workshop.demo.team.repository;

import com.opsgenie.workshop.demo.team.Team;

import java.util.List;
import java.util.Set;

public interface TeamRepository {

    List<Team> listTeams();
    Team getTeam(Team team);
    String addTeam(Team team);
    void updateTeam(Team team);
    void deleteTeam(Team team);
}
