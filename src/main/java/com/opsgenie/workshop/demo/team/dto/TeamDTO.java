package com.opsgenie.workshop.demo.team.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opsgenie.workshop.demo.team.Team;

import java.util.Set;

public class TeamDTO {
    @JsonProperty("id")
    public String id;

    @JsonProperty("name")
    public String name;

    @JsonProperty("userIds")
    public Set<String> userIds;

    public TeamDTO() {
    }

    public TeamDTO(String id, String name, Set<String> userIds) {
        this.id = id;
        this.name = name;
        this.userIds = userIds;
    }

    public Team toTeam() {
        return new Team(id, name, userIds);
    }
}
