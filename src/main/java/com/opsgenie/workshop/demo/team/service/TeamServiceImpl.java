package com.opsgenie.workshop.demo.team.service;


import com.opsgenie.workshop.demo.aws.AssignTeamMemberQueueProcessor;
import com.opsgenie.workshop.demo.aws.QueueMessageDTO;
import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.team.dto.TeamDTO;
import com.opsgenie.workshop.demo.team.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private AssignTeamMemberQueueProcessor assignTeamMemberQueueProcessor;

    @Override
    public List<Team> listTeams() {
        return teamRepository.listTeams();
    }

    @Override
    public Team getTeam(TeamDTO teamDTO) {
        Team team = teamDTO.toTeam();
        return teamRepository.getTeam(team);
    }

    @Override
    // @return: id of the added team
    public String addTeam(TeamDTO teamDTO) {
        Team team = teamDTO.toTeam();

        String uuid = UUID.randomUUID().toString();
        Team newTeam= new Team(uuid, team.getName(), team.getUserIds());

        return teamRepository.addTeam(newTeam);
    }

    @Override
    public void updateTeam(TeamDTO teamDTO) {
        Team team = teamDTO.toTeam();
        teamRepository.updateTeam(team);
    }

    @Override
    public void deleteTeam(TeamDTO teamDTO) {
        Team team = teamDTO.toTeam();
        teamRepository.deleteTeam(team);
    }

    @Override
    public void assignMember(String teamId, List<String> userIds) {
        try {
            assignTeamMemberQueueProcessor.sendAssignTeamMember(teamId, userIds);
        }
        catch (Exception e) {
            e.getStackTrace();
        }
    }

    @Override
    public void addMember(QueueMessageDTO queueMessageDTO) {
        Team team = getTeam(new TeamDTO(queueMessageDTO.getTeamId(), null, null));
        Set<String> memberIds;

        if (team == null)
            return;

        if (team.getUserIds() != null) {
            team.getUserIds().add(queueMessageDTO.getUserId());
            memberIds = team.getUserIds();
        }
        else {
            memberIds = new HashSet<String>();
            memberIds.add(queueMessageDTO.getUserId());
        }

        TeamDTO updateTeamDTO = new TeamDTO(team.getId(), team.getName(), memberIds);
        updateTeam(updateTeamDTO);
    }
}
