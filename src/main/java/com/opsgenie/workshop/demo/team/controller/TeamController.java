package com.opsgenie.workshop.demo.team.controller;


import com.opsgenie.workshop.demo.exceptions.EntityNotFoundException;
import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.team.dto.TeamDTO;
import com.opsgenie.workshop.demo.team.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "/teams")
public class TeamController {

    @Autowired
    public TeamService teamService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<TeamDTO> allTeams() {
        List<TeamDTO> teamDTOS = null;
        try {
            teamDTOS = teamService.listTeams().stream()
                    .map(team -> new TeamDTO(team.getId(), team.getName(), team.getUserIds()))
                    .collect(Collectors.toList());
        }
        catch (Exception e) {
            System.out.println(e.getStackTrace());
        }

        return teamDTOS;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TeamDTO teamWithId(@PathVariable("id") String id) {

        Team team;

        try {

            team = teamService.getTeam(new TeamDTO(id, "",Stream.<String>empty().collect(Collectors.toSet())));
        }
        catch (Exception e) {
            throw new EntityNotFoundException("No team found with given ID:" + id);
        }

        TeamDTO teamDTO = new TeamDTO(team.getId(), team.getName(), team.getUserIds());

        return teamDTO;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseBody TeamDTO addTeam(@RequestBody TeamDTO teamDTO) {
        String id = teamService.addTeam(teamDTO);
        return new TeamDTO(id, teamDTO.name, teamDTO.userIds);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateTeam(@RequestBody TeamDTO teamDTO, @PathVariable("id") String id) {
        try {
            teamService.updateTeam(new TeamDTO(id, teamDTO.name, teamDTO.userIds));
        }
        catch (Exception e) {
            throw new EntityNotFoundException("No team found with given ID:" + id);
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteTeam(@PathVariable("id") String id) {
        TeamDTO teamDTO = new TeamDTO(id, "",Stream.<String>empty().collect(Collectors.toSet()));
        try {
            teamService.deleteTeam(teamDTO);
        }
        catch (Exception e) {
            throw new EntityNotFoundException("No team found with given ID:" + id);
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}/assignTeamMember", method = RequestMethod.POST)
    public ResponseEntity assignTeamMember(@PathVariable("id") String teamId, @RequestBody List<String> userIds) {

        teamService.assignMember(teamId, userIds);

        return ResponseEntity.accepted().build();
    }

}
