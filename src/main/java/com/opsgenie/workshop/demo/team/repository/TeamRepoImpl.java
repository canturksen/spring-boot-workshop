package com.opsgenie.workshop.demo.team.repository;

import com.opsgenie.workshop.demo.team.Team;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TeamRepoImpl implements TeamRepository {

    List<Team> teams;

    {
        teams = new ArrayList<Team>();
                teams.add(new Team(UUID.randomUUID().toString(), "TeamA", Collections.emptySet()));
                teams.add(new Team(UUID.randomUUID().toString(), "TeamB", Collections.emptySet()));
                teams.add(new Team(UUID.randomUUID().toString(), "TeamC", Collections.emptySet()));
    }

    @Override
    public List<Team> listTeams() {
        return teams;
    }

    @Override
    public Team getTeam(Team team) {
        return teams.stream().filter(t -> t.getId().equals(team.getId())).findFirst().get();
    }

    @Override
    public String addTeam(Team team) {
        teams.add(team);
        return team.getId();
    }

    @Override
    public void updateTeam(Team team) {
        Team teamFound = getTeam(team);
        teamFound.setName(team.getName());
        teamFound.setUserIds(team.getUserIds());
    }

    @Override
    public void deleteTeam(Team team) {
        Team teamFound = getTeam(team);
        teams.remove(teamFound);
    }
}
