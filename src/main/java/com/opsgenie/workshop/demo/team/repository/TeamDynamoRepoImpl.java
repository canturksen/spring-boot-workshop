package com.opsgenie.workshop.demo.team.repository;


import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.opsgenie.workshop.demo.team.Team;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
@Primary
public class TeamDynamoRepoImpl implements TeamRepository {

    //AWSCredentials awsCredentials = new BasicAWSCredentials("Fake", "Fake");

    private AWSCredentials awsCredentials = new BasicAWSCredentials("fake", "fake");
    private AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

    private AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
            .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration("http://localhost:3966", "us-west-2"))
            .withCredentials(awsCredentialsProvider)
            .build();

    private DynamoDB dynamoDB = new DynamoDB(client);

    Table table = dynamoDB.getTable("OnboardingTeams");

    @Override
    public List<Team> listTeams() {
        List<Team> teamList = new ArrayList<>();
        ItemCollection<?> col = table.scan();
        for (Item item : col) {
            teamList.add(new Team((String) item.get("Id"), (String) item.get("name"), (Set<String>) item.get("userIds")));
        }
        return teamList;
    }

    @Override
    public Team getTeam(Team team) {
        Item item = table.getItem("Id", team.getId());
        return new Team((String) item.get("Id"), (String) item.get("name"), (Set<String>) item.get("userIds"));
    }

    @Override
    public String addTeam(Team team) {
        try {
            Item item = new Item()
                    .withPrimaryKey("Id", team.getId());

            if (!team.getName().isEmpty()) {
                item.withString("name", team.getName());
            }

            if (!team.getUserIds().isEmpty()) {
                item.withStringSet("userIds", team.getUserIds());
            }

            table.putItem(item);

        }
        catch (Exception e) {
            System.err.println("Create items failed.");
            System.err.println(e.getMessage());
        }

        return team.getId();
    }

    @Override
    public void updateTeam(Team team) {
        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey("Id", team.getId())
                .withAttributeUpdate(new AttributeUpdate("name").put(team.getName()));

        // better empty check ?
        if (team.getUserIds() != null && !team.getUserIds().isEmpty()) {
            updateItemSpec.addAttributeUpdate(new AttributeUpdate("userIds").addElements(team.getUserIds().toArray()));
        } else {
            updateItemSpec.addAttributeUpdate(new AttributeUpdate("userIds").delete());
        }

        try {
            table.updateItem(updateItemSpec);
        }
        catch (Exception e) {
            System.err.println("update item failed.");
            System.err.println(e.getMessage());
        }

    }

    @Override
    public void deleteTeam(Team team) {
        DeleteItemSpec deleteItemSpec = new DeleteItemSpec()
                .withPrimaryKey("Id", team.getId());

        try {
            table.deleteItem(deleteItemSpec);
        }
        catch (Exception e) {
            System.err.println("delete item failed.");
            System.err.println(e.getMessage());
        }

    }
}
