package com.opsgenie.workshop.demo.team.service;

import com.opsgenie.workshop.demo.aws.QueueMessageDTO;
import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.team.dto.TeamDTO;

import java.util.List;

public interface TeamService {

    List<Team> listTeams();
    Team getTeam(TeamDTO teamDTO);
    String addTeam(TeamDTO teamDTO);
    void updateTeam(TeamDTO teamDTO);
    void deleteTeam(TeamDTO teamDTO);
    void assignMember(String teamId, List<String> userId);
    void addMember(QueueMessageDTO queueMessageDTO);

}
