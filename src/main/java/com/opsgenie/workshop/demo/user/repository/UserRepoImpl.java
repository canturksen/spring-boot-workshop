package com.opsgenie.workshop.demo.user.repository;

import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.user.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Component
public class UserRepoImpl implements UserRepository {

    List<User> users;

    {
        users = new ArrayList<User>();
        users.add(new User().setId(UUID.randomUUID().toString()).setName("UserA"));
        users.add(new User().setId(UUID.randomUUID().toString()).setName("UserB"));
        users.add(new User().setId(UUID.randomUUID().toString()).setName("UserC"));
    }

    @Override
    public List<User> listUsers() {
        return users;
    }

    @Override
    public User getUser(User user) {
        return users.stream().filter(u -> u.getId().equals(user.getId())).findFirst().get();
    }

    @Override
    public String addUser(User user) {
        users.add(user);
        return user.getId();
    }

    @Override
    public void updateUser(User user) {
        User userFound = getUser(user);
        userFound.setName(user.getName());
    }

    @Override
    public void deleteUser(User user) {
        User userFound = getUser(user);
        users.remove(userFound);
    }
}
