package com.opsgenie.workshop.demo.user.repository;

import com.opsgenie.workshop.demo.user.User;

import java.util.List;

public interface UserRepository {

    List<User> listUsers();

    User getUser(User user);

    String addUser(User user);

    void updateUser(User user);

    void deleteUser(User user);
}
