package com.opsgenie.workshop.demo.user.repository;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.opsgenie.workshop.demo.team.Team;
import com.opsgenie.workshop.demo.user.User;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
@Primary
public class UserDynamoRepoImpl implements UserRepository {
    private AWSCredentials awsCredentials = new BasicAWSCredentials("fake", "fake");
    private AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

    private AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
            .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration("http://localhost:3966", "us-west-2"))
            .withCredentials(awsCredentialsProvider)
            .build();

    private DynamoDB dynamoDB = new DynamoDB(client);

    Table table = dynamoDB.getTable("OnboardingUsers");

    @Override
    public List<User> listUsers() {
        List<User> userList = new ArrayList<>();
        ItemCollection<?> col = table.scan();
        for (Item item : col) {
            userList.add(
                    new User().setId((String) item.get("Id")).setName((String) item.get("name"))
            );
        }
        return userList;
    }

    @Override
    public User getUser(User user) {
        Item item = table.getItem("Id", user.getId());
        return new User().setId((String) item.get("Id")).setName((String) item.get("name"));
    }

    @Override
    public String addUser(User user) {
        try {
            Item item = new Item()
                    .withPrimaryKey("Id", user.getId());

            if (!user.getName().isEmpty()) {
                item.withString("name", user.getName());
            }

            table.putItem(item);

        }
        catch (Exception e) {
            System.err.println("Create items failed.");
            System.err.println(e.getMessage());
        }

        return user.getId();
    }

    @Override
    public void updateUser(User user) {
        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey("Id", user.getId())
                .withAttributeUpdate(new AttributeUpdate("name").put(user.getName()));

        try {
            table.updateItem(updateItemSpec);
        }
        catch (Exception e) {
            System.err.println("update item failed.");
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void deleteUser(User user) {
        DeleteItemSpec deleteItemSpec = new DeleteItemSpec()
                .withPrimaryKey("Id", user.getId());

        try {
            table.deleteItem(deleteItemSpec);
        }
        catch (Exception e) {
            System.err.println("delete item failed.");
            System.err.println(e.getMessage());
        }
    }
}
