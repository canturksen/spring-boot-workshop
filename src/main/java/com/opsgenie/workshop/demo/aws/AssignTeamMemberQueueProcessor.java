package com.opsgenie.workshop.demo.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opsgenie.workshop.demo.team.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.List;

@Component
@PropertySource("classpath:aws.properties")
public class AssignTeamMemberQueueProcessor {

    private AmazonSQS sqs;
    private String queueUrl;

    @Autowired
    private TeamService teamService;

    @Value("${aws.client.accessKey}")
    private String awsAccessKey;

    @Value("${aws.client.secretKey}")
    private String awsSecretAccessKey;


    @PostConstruct
    public void init() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretAccessKey);
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

        sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion("eu-west-1").build();

        queueUrl = sqs.getQueueUrl("team_assign_team_member_queue").getQueueUrl();
    }

    @PreDestroy
    public void destroy() {
        if (sqs != null) {
            sqs.shutdown();
        }
    }


    public void sendAssignTeamMember(String teamId, List<String> userIds) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        for (String userId : userIds) {
            String temp = mapper.writeValueAsString(new QueueMessageDTO().setTeamId(teamId).setUserId(userId));

            SendMessageRequest send_msg_request = new SendMessageRequest()
                    .withQueueUrl(queueUrl)
                    .withMessageBody(temp)
                    .withDelaySeconds(5);
            sqs.sendMessage(send_msg_request);
        }
    }

    @Scheduled(fixedDelay = 1000)
    public void receiveMessages() throws IOException {
        List<Message> messages = sqs.receiveMessage(queueUrl).getMessages();

        // delete messages from the queue
        for (Message m : messages) {
            sqs.deleteMessage(queueUrl, m.getReceiptHandle());

            ObjectMapper mapper = new ObjectMapper();
            QueueMessageDTO queueMessageDTO = mapper.readValue(m.getBody(), QueueMessageDTO.class);
            teamService.addMember(queueMessageDTO);
        }
    }
}
