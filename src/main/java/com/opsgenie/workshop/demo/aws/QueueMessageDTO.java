package com.opsgenie.workshop.demo.aws;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class QueueMessageDTO {

    @JsonProperty("teamId")
    private String teamId;

    @JsonProperty("userId")
    private String userId;

    public String getTeamId() {
        return teamId;
    }

    public QueueMessageDTO setTeamId(String teamId) {
        this.teamId = teamId;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public QueueMessageDTO setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    @Override
    public String toString() {
        return "QueueMessageDTO teamId: " + teamId + " userId: " + userId;
    }
}
